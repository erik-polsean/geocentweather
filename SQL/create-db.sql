USE master
GO

IF (EXISTS (SELECT name FROM master.sys.sysdatabases WHERE name = N'GeocentWeather'))
BEGIN
	DROP DATABASE GeocentWeather;
END

CREATE DATABASE GeocentWeather;
GO

USE GeocentWeather
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Weather'))
BEGIN
	DROP TABLE GeocentWeather.dbo.Weather;
END

CREATE TABLE Weather (
	[Id] INT IDENTITY(1,1) PRIMARY KEY,
	[Date] DATE NOT NULL,
	[Latitude] DECIMAL(8,6) NOT NULL,
	[Longitude] DECIMAL(9,6) NOT NULL,
	[City] NVARCHAR(255) NOT NULL,
	[State] NVARCHAR(100) NOT NULL,
	[Temperature] DECIMAL(5,2) NOT NULL
);
GO

INSERT INTO Weather ([Date], Latitude, Longitude, City, [State], Temperature)
VALUES (GETDATE(), 38.705000, -77.233611, 'Lorton', 'Virginia', 75.1),
	   (GETDATE(), 38.643611, -77.260833, 'Woodbridge', 'Virginia', 74.8),
	   (GETDATE(), 38.852500, -77.304167, 'Fairfax', 'Virginia', 76.2),
	   (GETDATE(), 38.751389, -77.476389, 'Manassas', 'Virginia', 75.7),
	   (GETDATE(), 38.971389, -77.388611, 'Herndon', 'Virginia', 76.8),
	   (GETDATE(), 38.954444, -77.346389, 'Reston', 'Virginia', 77.3);
GO

SELECT * FROM Weather;
GO
﻿using GeocentWeather.DAL.Interfaces.Persistance.Repositories;
using System;

namespace GeocentWeather.DAL.Interfaces.Persistance
{
    public interface IUnitOfWork : IDisposable
    {
        IWeatherRepository Weather { get; }

        int Complete();
    }
}

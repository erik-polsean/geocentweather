﻿using System;
using System.Data.Entity;

namespace GeocentWeather.DAL.Interfaces.Persistance
{
    public interface IWeatherDbContext : IDisposable
    {
        int SaveChanges();

        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}

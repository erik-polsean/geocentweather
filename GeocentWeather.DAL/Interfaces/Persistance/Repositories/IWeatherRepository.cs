﻿using GeocentWeather.DAL.Entities;

namespace GeocentWeather.DAL.Interfaces.Persistance.Repositories
{
    public interface IWeatherRepository : IRepository<WeatherEntity>
    {
    }
}

﻿using GeocentWeather.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace GeocentWeather.DAL.Interfaces.Persistance.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        T Create(T entity);

        T Get(Expression<Func<T, bool>> query);

        T GetById(int id);

        List<T> GetAll();

        void Delete(T entity);
    }
}

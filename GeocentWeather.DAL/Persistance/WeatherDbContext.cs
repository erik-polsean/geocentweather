using System.Data.Entity;
using GeocentWeather.DAL.Entities;
using GeocentWeather.DAL.Interfaces.Persistance;

namespace GeocentWeather.DAL.Persistance
{
    public partial class WeatherDbContext : DbContext, IWeatherDbContext
    {
        public WeatherDbContext()
            : base("name=WeatherDbContext")
        {
            Database.SetInitializer<WeatherDbContext>(null);
        }

        int IWeatherDbContext.SaveChanges()
        {
            return base.SaveChanges();
        }

        IDbSet<TEntity> IWeatherDbContext.Set<TEntity>()
        {
            return Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WeatherEntity>()
                .Property(e => e.Latitude)
                .HasPrecision(8, 6);

            modelBuilder.Entity<WeatherEntity>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<WeatherEntity>()
                .Property(e => e.Temperature)
                .HasPrecision(5, 2);
        }
    }
}

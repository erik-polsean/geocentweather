﻿using GeocentWeather.DAL.Entities;
using GeocentWeather.DAL.Interfaces.Persistance;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace GeocentWeather.DAL.Persistance.Repositories
{
    public abstract class BaseRepository<T> where T : Entity
    {
        protected readonly IWeatherDbContext dbContext;
        protected readonly IDbSet<T> entities;

        protected BaseRepository(IWeatherDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.entities = dbContext.Set<T>();
        }

        public T Create(T entity)
        {
            return entities.Add(entity);
        }

        public T Get(Expression<Func<T, bool>> query)
        {
            return this.entities.Where(query).FirstOrDefault();
        }

        public T GetById(int id)
        {
            return this.entities.Where(e => e.Id == id).FirstOrDefault();
        }

        public List<T> GetAll()
        {
            return this.entities.OrderBy(e => e.Id).ToList();
        }

        public void Delete(T entity)
        {
            this.entities.Remove(entity);
        }
    }
}

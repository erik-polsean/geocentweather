﻿using GeocentWeather.DAL.Entities;
using GeocentWeather.DAL.Interfaces.Persistance;
using GeocentWeather.DAL.Interfaces.Persistance.Repositories;

namespace GeocentWeather.DAL.Persistance.Repositories
{
    public class WeatherRepository : BaseRepository<WeatherEntity>, IWeatherRepository
    {
        public WeatherRepository(IWeatherDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}

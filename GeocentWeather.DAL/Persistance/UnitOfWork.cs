﻿using GeocentWeather.DAL.Interfaces.Persistance;
using GeocentWeather.DAL.Interfaces.Persistance.Repositories;
using GeocentWeather.DAL.Persistance.Repositories;
using System;

namespace GeocentWeather.DAL.Persistance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IWeatherDbContext dbContext;
        private bool isDisposed = false;

        public IWeatherRepository Weather { get; private set; }

        public UnitOfWork(IWeatherDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.Weather = new WeatherRepository(dbContext);
        }

        public int Complete()
        {
            return this.dbContext.SaveChanges();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this.isDisposed)
            {
                this.dbContext.Dispose();
            }

            this.isDisposed = true;
        }
    }
}

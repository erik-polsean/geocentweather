using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeocentWeather.DAL.Entities
{
    [Table("Weather")]
    public partial class WeatherEntity : Entity
    {
        [Column(TypeName = "Date")]
        public DateTime Date { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(100)]
        public string State { get; set; }

        public decimal Temperature { get; set; }
    }
}

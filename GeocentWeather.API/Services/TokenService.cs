﻿using GeocentWeather.API.Interfaces;
using GeocentWeather.API.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace GeocentWeather.API.Services
{
    public class TokenService : ITokenService
    {
        private readonly JwtSecurityTokenHandler TokenHandler;
        private readonly byte[] SecretKey;

        public TokenService()
        {
            this.TokenHandler = new JwtSecurityTokenHandler();
            this.SecretKey = Encoding.ASCII.GetBytes("geocent_secret_weather_key_12345");
        }

        public bool Authenticate(User user)
        {
            return user.Password == this.ReverseString(user.Username);
        }

        public string NewToken(User user)
        {
            var claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, user.Username)
            };

            var issuedAt = DateTime.UtcNow;
            var expiresAt = DateTime.UtcNow.AddMinutes(30);
            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(this.SecretKey), SecurityAlgorithms.HmacSha256Signature);

            var token = this.TokenHandler.CreateJwtSecurityToken
            (
                "https://localhost:44333", 
                "https://localhost:44333", 
                new ClaimsIdentity(claims), 
                issuedAt, expiresAt, 
                signingCredentials: signingCredentials
            );
            
            return this.TokenHandler.WriteToken(token);
        }

        public ClaimsPrincipal VerifyToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidAudience = "https://localhost:44333",
                ValidIssuer = "https://localhost:44333",
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                LifetimeValidator = this.LifetimeValidator,
                IssuerSigningKey = new SymmetricSecurityKey(this.SecretKey),
                ClockSkew = TimeSpan.Zero
            };

            return this.TokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
        }

        private bool LifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            return expires != null && DateTime.UtcNow < expires;
        }

        private string ReverseString(string input)
        {
            string output = "";
            var reversedInput = input.Reverse();

            foreach (var letter in reversedInput)
            {
                output += letter;
            }

            return output;
        }
    }
}
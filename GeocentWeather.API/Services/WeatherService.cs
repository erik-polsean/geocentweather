﻿using GeocentWeather.API.Interfaces;
using GeocentWeather.DAL.Entities;
using GeocentWeather.DAL.Interfaces.Persistance;
using System.Collections.Generic;

namespace GeocentWeather.API.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly IUnitOfWork unitOfWork;

        public WeatherService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public WeatherEntity Create(WeatherEntity entity)
        {
            var newEntity = this.unitOfWork.Weather.Create(entity);
            this.unitOfWork.Complete();

            return newEntity;
        }

        public WeatherEntity GetById(int id)
        {
            return this.unitOfWork.Weather.GetById(id);
        }

        public List<WeatherEntity> GetAll()
        {
            return this.unitOfWork.Weather.GetAll();
        }

        public void Delete(int id)
        {
            var entity = this.GetById(id);

            this.unitOfWork.Weather.Delete(entity);
            this.unitOfWork.Complete();
        }
    }
}
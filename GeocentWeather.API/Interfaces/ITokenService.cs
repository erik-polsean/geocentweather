﻿using GeocentWeather.API.Models;
using System.Security.Claims;

namespace GeocentWeather.API.Interfaces
{
    public interface ITokenService
    {
        bool Authenticate(User user);

        string NewToken(User user);

        ClaimsPrincipal VerifyToken(string token);
    }
}

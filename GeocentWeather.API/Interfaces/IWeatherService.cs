﻿using GeocentWeather.DAL.Entities;
using System.Collections.Generic;

namespace GeocentWeather.API.Interfaces
{
    public interface IWeatherService
    {
        WeatherEntity Create(WeatherEntity entity);

        WeatherEntity GetById(int id);

        List<WeatherEntity> GetAll();

        void Delete(int id);
    }
}

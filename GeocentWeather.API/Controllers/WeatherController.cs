﻿using GeocentWeather.API.Filters;
using GeocentWeather.API.Interfaces;
using GeocentWeather.API.Utilities.Messages.Controller;
using GeocentWeather.DAL.Entities;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GeocentWeather.API.Controllers
{
    /// <summary>
    /// Controller for Weather objects.
    /// </summary>
    [RoutePrefix("weather")]
    public class WeatherController : ApiController
    {
        private readonly IWeatherService weatherService;

        /// <summary>
        /// Constructor for the Weather controller.
        /// </summary>
        /// <param name="weatherService"></param>
        public WeatherController(IWeatherService weatherService)
        {
            this.weatherService = weatherService;
        }

        /// <summary>
        /// Create a Weather object.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("", Name = "Create")]
        [SwaggerResponse(HttpStatusCode.Created, WeatherControllerMessages.SuccessfulCreate, Type = typeof(WeatherEntity))]
        [SwaggerResponse(HttpStatusCode.Forbidden, WeatherControllerMessages.ForbiddenToCreate)]
        [SwaggerResponse(HttpStatusCode.InternalServerError, WeatherControllerMessages.InternalServerErrorCreate)]
        [SwaggerOperation(WeatherControllerMessages.OperationCreateSummary)]
        [SwaggerOperationFilter(typeof(AddHeaderParameterFilter))]
        [SwaggerResponseRemoveDefaults]
        public IHttpActionResult Create(WeatherEntity entity)
        {
            var newEntity = this.weatherService.Create(entity);
            return this.CreatedAtRoute("Create", new { id = newEntity.Id }, newEntity);
        }

        /// <summary>
        /// Get all Weather objects.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, WeatherControllerMessages.SuccessfulGetAll, Type = typeof(List<WeatherEntity>))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, WeatherControllerMessages.InternalServerErrorGetAll)]
        [SwaggerOperation(WeatherControllerMessages.OperationGetAllSummary)]
        public IHttpActionResult GetAll()
        {
            try
            {
                return this.Ok(this.weatherService.GetAll());
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        /// <summary>
        /// Get a Weather object.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        [SwaggerResponse(HttpStatusCode.OK, WeatherControllerMessages.SuccessfulGetById, Type = typeof(WeatherEntity))]
        [SwaggerResponse(HttpStatusCode.NotFound, WeatherControllerMessages.EntityNotFound)]
        [SwaggerResponse(HttpStatusCode.InternalServerError, WeatherControllerMessages.InternalServerErrorGetById)]
        [SwaggerOperation(WeatherControllerMessages.OperationGetByIdSummary)]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var entity = this.weatherService.GetById(id);
                if (entity != null && entity != default(WeatherEntity))
                {
                    return this.Ok(entity);
                }
                else
                {
                    return this.NotFound();
                }
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a Weather object.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{id:int}")]
        [SwaggerResponse(HttpStatusCode.NoContent, WeatherControllerMessages.SuccessfulDelete)]
        [SwaggerResponse(HttpStatusCode.NotFound, WeatherControllerMessages.EntityNotFound)]
        [SwaggerResponse(HttpStatusCode.Forbidden, WeatherControllerMessages.ForbiddenToDelete)]
        [SwaggerResponse(HttpStatusCode.InternalServerError, WeatherControllerMessages.InternalServerErrorDelete)]
        [SwaggerOperation(WeatherControllerMessages.OperationDeleteSummary)]
        [SwaggerOperationFilter(typeof(AddHeaderParameterFilter))]
        [SwaggerResponseRemoveDefaults]
        public IHttpActionResult Delete(int id)
        {
            var entity = this.weatherService.GetById(id);
            if (entity != null && entity != default(WeatherEntity))
            {
                this.weatherService.Delete(id);
                return this.ResponseMessage(new HttpResponseMessage(HttpStatusCode.NoContent));
            }
            else
            {
                return this.NotFound();
            }
        }
    }
}

﻿using GeocentWeather.API.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System;
using System.Web.Http;
using GeocentWeather.API.Interfaces;

namespace GeocentWeather.API.Controllers
{
    public class UserController : ApiController
    {
        private readonly ITokenService tokenService;

        public UserController(ITokenService tokenService)
        {
            this.tokenService = tokenService;
        }

        /// <summary>
        /// Authenicates a User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(User user)
        {
            if (this.tokenService.Authenticate(user))
            {
                return this.Ok(new { Token = this.tokenService.NewToken(user) });
            }
            else
            {
                return this.Unauthorized();
            }
        }
    }
}

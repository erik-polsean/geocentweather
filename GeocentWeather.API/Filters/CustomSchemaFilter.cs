﻿using GeocentWeather.API.Models;
using GeocentWeather.DAL.Entities;
using Marvin.JsonPatch;
using Newtonsoft.Json;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeocentWeather.API.Filters
{
    public class CustomSchemaFilter : ISchemaFilter
    {
        public void Apply(Schema schema, SchemaRegistry schemaRegistry, Type type)
        {
            List<JsonPatch> patchOps = null;

            if (type == typeof(JsonPatchDocument<WeatherEntity>))
            {
                patchOps = this.GetWeatherPatchOps();
            }

            if (patchOps != null)
            {
                var json = JsonConvert.SerializeObject(patchOps, Formatting.Indented);
                schema.example = json;
            }
        }

        private List<JsonPatch> GetWeatherPatchOps()
        {
            return new List<JsonPatch>
            {
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.Id)}", value = "1"},
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.Date)}", value = "2000-01-01T00:00:00"},
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.Latitude)}", value = "40.712778"},
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.Longitude)}", value = "-74.006111"},
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.City)}", value = "New York City"},
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.State)}", value = "New York"},
                new JsonPatch { op = "replace", path = $"/{nameof(WeatherEntity.Temperature)}", value = "49.00"}
            };
        }
    }
}
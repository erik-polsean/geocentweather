using System.Web.Http;
using Swashbuckle.Application;
using System.Web;
using GeocentWeather.API.Filters;

namespace GeocentWeather.API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SchemaFilter<CustomSchemaFilter>();
                        c.SingleApiVersion("v1", "GeocentWeather.API");
                        c.PrettyPrint();
                        c.IncludeXmlComments(HttpContext.Current.Server.MapPath("bin\\GeocentWeather.API.xml"));
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.DocExpansion(DocExpansion.List);
                    });
        }
    }
}

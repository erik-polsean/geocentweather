﻿using Autofac;
using Autofac.Integration.WebApi;
using GeocentWeather.API.Interfaces;
using GeocentWeather.API.Services;
using GeocentWeather.DAL.Interfaces.Persistance;
using GeocentWeather.DAL.Persistance;
using System.Reflection;
using System.Web.Http;

namespace GeocentWeather.API
{
    public static class AutoFacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            RegisterClasses(builder);

            var container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterClasses(ContainerBuilder builder)
        {
            RegisterDatabase(builder);
            RegisterServices(builder);
        }

        private static void RegisterDatabase(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherDbContext>().As<IWeatherDbContext>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherService>().As<IWeatherService>().InstancePerRequest();
            builder.RegisterType<TokenService>().As<ITokenService>().InstancePerRequest();
        }
    }
}
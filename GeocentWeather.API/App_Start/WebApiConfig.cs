﻿using GeocentWeather.API.Interfaces;
using GeocentWeather.API.Security;
using GeocentWeather.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace GeocentWeather.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new TokenValidationHandler(new TokenService()));

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeocentWeather.API.Utilities.Messages
{
    public static class ForbiddenMessage
    {
        public const string Pfx = "User is not authorized to ";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeocentWeather.API.Utilities.Messages
{
    public static class InternalServerErrorMessage
    {
        public const string Pfx = "An internal server error occured when trying to ";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeocentWeather.API.Utilities.Messages
{
    public static class EntityCreatedMessage
    {
        public const string Pfx = "The ";
        public const string Sfx = " has been created.";
    }
}
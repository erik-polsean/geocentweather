﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeocentWeather.API.Utilities.Messages.Controller
{
    public static class WeatherControllerMessages
    {
        public const string ForbiddenToCreate = ForbiddenMessage.Pfx + "create " + EntityName.Weather + ".";
        public const string ForbiddenToDelete = ForbiddenMessage.Pfx + "delete " + EntityName.Weather + ".";

        public const string SuccessfulCreate = "A new " + EntityName.Weather + " has been created.";
        public const string SuccessfulDelete = "The " + EntityName.Weather + " has been deleted.";
        public const string SuccessfulGetAll = "A list of " + EntityName.Weather + " entities.";
        public const string SuccessfulGetById = "A single " + EntityName.Weather + " entity found by Id.";

        public const string InternalServerErrorCreate = InternalServerErrorMessage.Pfx + "create a " + EntityName.Weather + " entity.";
        public const string InternalServerErrorDelete = InternalServerErrorMessage.Pfx + "delete a " + EntityName.Weather + " entity.";
        public const string InternalServerErrorGetAll = InternalServerErrorMessage.Pfx + "get all " + EntityName.Weather + " entities.";
        public const string InternalServerErrorGetById = InternalServerErrorMessage.Pfx + "get a " + EntityName.Weather + " entity by Id.";

        public const string EntityNotFound = NotFoundMessage.Pfx + EntityName.Weather + NotFoundMessage.Sfx;

        public const string OperationCreateSummary = "Create a new " + EntityName.Weather + " entity.";
        public const string OperationDeleteSummary = "Delete a " + EntityName.Weather + " entity.";
        public const string OperationGetAllSummary = "Get all " + EntityName.Weather + " entities.";
        public const string OperationGetByIdSummary = "Get a " + EntityName.Weather + " entity by Id.";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeocentWeather.API.Utilities.Messages
{
    public static class NotFoundMessage
    {
        public const string Pfx = "No ";
        public const string Sfx = " was found with the given Id.";
    }
}
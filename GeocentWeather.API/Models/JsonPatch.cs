﻿namespace GeocentWeather.API.Models
{
    public class JsonPatch
    {
        public string op;
        public string path;
        public string value;
    }
}